# 🏳️‍🌈🏳️‍⚧️ programming-socks-highlight.nvim 🏳️‍⚧️🏳️‍🌈
The Programming Socks Highlighter is a modern Neovim plugin designed to semantically highlight various aspects of your code - variables, type names, function names, and similar - such that different names carry different colours, and hence you can find nearby usages of the same variables (and locate typos *in* variables) just by looking at the colours. 

Here are some existing resources on this concept:
* The (as far as I'm aware) [original blogpost](https://medium.com/@evnbr/coding-in-color-3a6db2743a1e) by Evan Brooks proposing this idea.
* The [semantic-highlight.vim](https://github.com/jaxbot/semantic-highlight.vim) plugin, which uses vim regex syntaxes and requires a gigantic pile of manual colours in most cases. This is what I used in my vim config, and it works well, but creating a long-enough colour list is slow and if you change colour scheme it can cause difficulties with contrast. 
* The [neovim-colorcoder](https://github.com/blahgeek/neovim-colorcoder/tree/master/plugin) plugin. I haven't tried this one, and it certainly seems decent, though it is insufficiently granular for my needs. 

This is inspired by those resources and existing plugins, though intends to improve on them by allowing use of Language Server Protocol semantic tokens, and (perhaps in the near future) tree-sitter rules as well, and is designed to avoid conflicting colours by reading your colour schemes and intelligently selecting colours that are different to the values specified *and* different from existing selected colours.

The Programming Socks Highlighter is licensed under AGPL version 3 or later.
