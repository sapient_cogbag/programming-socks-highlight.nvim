--- Module containing the mathematics for actually generating new colours in an appropriate
--- and efficient manner.
local colourspaces = {}

colourspaces.coordinates = require("programming-socks-highlight.colourspaces.coordinates") 
colourspaces.exclusion = require("programming-socks-highlight.colourspaces.exclusion")

return colourspaces

-- programming-socks-highlight.nvim
-- Copyright (C) 2023  sapient_cogbag <sapient_cogbag at protonmail dot com>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published
-- by the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
