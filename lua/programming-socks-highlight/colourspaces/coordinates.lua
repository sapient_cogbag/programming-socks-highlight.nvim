--- Module that lets you define coordinate spaces for colours (e.g. rgb/hsv/etc.)
--- with custom distance metrics for a given colour point along each axis.
--- TODO: Use the LuaJIT c FFI library for SPEEEEEED - see: 
--- @alias psh.colourspaces.coordinates.Vector number[] vectorized colour version for a
---   given colour space
local coordinates = {}

--- @class psh.colourspaces.coordinates.Axis<ColourRepresentation>
--- @field distance_weight fun(colour: ColourRepresentation): number  How to weight
---  distances along this axis within the area nearby a given point. This MUST
---  produce the same value when given the same colour.
--- @field coordinate_retriever fun(colour: ColourRepresentation): number
--- @field partial_coordinate_retriever? fun(colour: ColourRepresentation): number?
---   If defined, then this function provides a way to allow for partially specified coordinates
---   within an altered variant of the representation. Used for finding distances to non-pointlike
---   areas
--- @field lower_bound number 
--- @field upper_bound number
--- @field index integer? If specified then this provides a specific index (1 based, like all lua),
---  and if this is present then the representation can also be a vector.
coordinates.axis = {}

---Construct a coordinate axis for a given colour space.
---@generic ColourRepresentation
---@param distance_weight number|fun(colour: ColourRepresentation): number
--- This can either use a location-dependent axis-weight based distance function,
--- or a constant value to multiply the difference along each axis.
--- The function MUST return the same value every time when given the same colour. i.e.
--- it must be a pure function.
---@param coordinate_retriever fun(colour: ColourRepresentation): number
---  How to obtain the value along this axis from whatever colour representation you
---  are using
---@param lower_bound number
---@param upper_bound number
---@param partial_coordinate_retriever? fun(colour: ColourRepresentation): number? See
---  type information for details, but this is required to represent non-point-like
---  structures.
---@return psh.colourspaces.coordinates.Axis<`ColourRepresentation`>
function coordinates.axis.new(distance_weight, coordinate_retriever, lower_bound, upper_bound,
                              partial_coordinate_retriever)
    if type(distance_weight) == "number" then
        local new_distance_weight = function(_colour)
            return distance_weight
        end
        distance_weight = new_distance_weight
    end
    return setmetatable({
        distance_weight = distance_weight,
        coordinate_retriever = coordinate_retriever,
        lower_bound = lower_bound,
        upper_bound = upper_bound,
        partial_coordinate_retriever = partial_coordinate_retriever
    }, { __index = coordinates.axis })
end

--- Create an axis that uses bare vectors/arraytables for it's representation.
--- Automatically implies a specific coordinate retriever and allows for partial coordinates
--- to represent non-pointlike colour space items nya
--- @param distance_weight number|fun(colour: number[]): number
---@param lower_bound number Minimum value along this axis.
---@param upper_bound number Maximum value along this axis.
---@param one_based_index integer
--- @return psh.colourspaces.coordinates.Axis<number[]>
function coordinates.axis.new_vectored(distance_weight, lower_bound, upper_bound, one_based_index)
    if type(distance_weight) == "number" then
        local new_distance_weight = function(_colour_representation)
            return distance_weight
        end
        distance_weight = new_distance_weight
    end
    return setmetatable(
        {
            distance_weight = distance_weight,
            coordinate_retriever = function(colour_vec)
                return colour_vec[one_based_index]
            end,
            partial_coordinate_retriever = function(colour_vec_table)
                return colour_vec_table[one_based_index]
            end,
            lower_bound = lower_bound,
            upper_bound = upper_bound,
            index = one_based_index
        }, { __index = coordinates.axis })
end

function coordinates.axis:get_upper_bound()
    return self.upper_bound
end

function coordinates.axis:get_lower_bound()
    return self.lower_bound
end

function coordinates.axis:is_vectored()
    return self.index ~= nil
end

-- Get the index if the representation is vector-like
---@return integer?
function coordinates.axis:vector_index()
    return self.index
end

--- @generic ColourRepresentation
--- @param colour ColourRepresentation
--- @return number # The value of the colour along the given axis.
function coordinates.axis:component_of(colour)
    return self.coordinate_retriever(colour)
end

function coordinates.axis:get_coordinate_retriever_function()
    return self.coordinate_retriever
end

function coordinates.axis:get_distance_weight_function()
    return self.distance_weight
end

--- @return nil|fun(colour: ColourRepresentation): number?
function coordinates.axis:get_partial_coordinate_retriever_function()
    return self.partial_coordinate_retriever
end

--- @generic ColourRepresentation
--- @param local_point ColourRepresentation
--- @return number local_axis_weight local distance weighting of this axis at this point.
function coordinates.axis:weight_at(local_point)
    return self.distance_weight(local_point)
end

--- Represents a unique coordinate system with named axes and a specific representation.
--- The vectors generated by this have a consistent order of the axes, but this order is
--- *not* specified.
--- @class psh.colourspaces.coordinates.System<ColourRepresentation>
--- @field private named_axes { [string]: psh.colourspaces.coordinates.Axis<ColourRepresentation> }
--- @field private vector_ordering { [string]: number } Provides a consistent ordering for the axes
--- A coordinate system is a collection of named axes. For simplicity, and because
--- lua HAS NO REAL TYPES :( nya
--- @field private axis_count number Number of axes - used to efficiently make vectors of
--- values.
--- @field private representation_is_vectorish bool If true then the representation is just
--- an array of numbers and extensive translation efforts are unnecessary (and the order is
--- specified via the axes index fields)
--- @field private ordered_coordinate_retrievers (fun(colour: ColourRepresentation): number)[]
---  Coordinate retrievers in vector order
--- @field private ordered_distance_weights (fun(colour: ColourRepresentation): number)[]
---  Distance weight functions in vector order >.< nya
--- @field private ordered_partial_coordinate_retrievers? (fun(colour: ColourRepresentation): number?)[]
---  Nil if not all of the partial coordinate extractors exist, an array otherwise.
--- @field private has_partial_coordinate_functionality boolean
---  Whether or not this has the functionality to use partial coordinates - i.e. the accessors are
---  all non-nil ^.^
--- @field private ordered_axes psh.colourspaces.coordinates.Axis<ColourRepresentation>[] axes in vector-order.
coordinates.system = {}

--- @generic ColourRepresentation
--- @param axes { [string]: psh.colourspaces.coordinates.Axis<ColourRepresentation> }
--- @return psh.colourspaces.coordinates.System<`ColourRepresentation`>
function coordinates.system.new(axes)
    local all_axes_are_vectorish = true
    --- @type table<integer, boolean>
    local defined_numerical_axes = {}
    for axis_name, axis_spec in pairs(axes) do
        local vec_idx = axis_spec:vector_index()
        if vec_idx ~= nil then
            if defined_numerical_axes[vec_idx] then
                error("duplicate vector index " .. tostring(vec_idx))
            end
            defined_numerical_axes[vec_idx] = true
        else
            all_axes_are_vectorish = false
            break
        end
    end

    local order = {}
    local idx = 1
    for axis_name, axis_spec in pairs(axes) do
        if not all_axes_are_vectorish then
            order[axis_name] = idx
        else
            order[axis_name] = axis_spec:vector_index()
        end
        idx = idx + 1
    end
    local total_axes = idx - 1

    local inverse_order = vim.tbl_add_reverse_lookup(vim.deepcopy(order))
    if #inverse_order ~= total_axes then
        error("Somehow there's been a gap in the ordering aftwer index " .. tostring(#inverse_order))
    end

    local vector_order_coordinate_extractors = {}
    local vector_order_partial_coordinate_extractors = {}
    local vector_order_distance_weights = {}
    local vector_order_axes = {}
    for idx, axis_name in ipairs(inverse_order) do
        vector_order_axes[idx] = axes[axis_name]
        vector_order_distance_weights[idx] = axes[axis_name]:get_distance_weight_function()
        vector_order_partial_coordinate_extractors[idx] = axes[axis_name]:get_partial_coordinate_retriever_function()
        vector_order_coordinate_extractors[idx] = axes[axis_name]:get_coordinate_retriever_function()
    end

    local has_all_partial_retrievers = true
    for idx = 1, total_axes do
        local has_extractor = vector_order_partial_coordinate_extractors[idx] ~= nil
        has_all_partial_retrievers = has_all_partial_retrievers and has_extractor
    end
    if not has_all_partial_retrievers then
        vector_order_partial_coordinate_extractors = nil
    end

    return setmetatable({
        named_axes = axes,
        vector_ordering = order,
        axis_count = total_axes,
        representation_is_vectorish = all_axes_are_vectorish,
        ordered_coordinate_retrievers = vector_order_coordinate_extractors,
        ordered_partial_coordinate_retrievers = vector_order_partial_coordinate_extractors,
        ordered_distance_weights = vector_order_distance_weights,
        has_partial_coordinate_functionality = has_all_partial_retrievers,
        ordered_axes = vector_order_axes
    }, { __index = coordinates.system })
end

--- Build a vector of numbers from the representation, with an arbitrary but
--- consistent order. If it is a vectorish representation, and the object is
--- just a table, then this should return the argument again ^.^
--- @generic ColourRepresentation
--- @param  colour ColourRepresentation
--- @return psh.colourspaces.coordinates.Vector coordinate_vector
function coordinates.system:make_vector(colour)
    if self.representation_is_vectorish and type(colour) == "table" then
        return colour
    end
    return vim.tbl_map(function(extractor) return extractor(colour) end, self.ordered_coordinate_retrievers)
end

--- Build a vector that can also be accessed by using the names of the axes as keys.
--- @generic ColourRepresentation
--- @param colour ColourRepresentation
--- @return table
function coordinates.system:make_tagged_vector(colour)
    local vector = self:make_vector(colour)
    return setmetatable(vector, {
        -- Only called for non-nil values
        __index = function(vec, key)
            if type(key) ~= "number" then
                local vector_index = self.vector_ordering[key]
                return rawget(vec, vector_index)
            else
                return rawget(vec, key)
            end
        end,
        --- Only called for non-nil contained values, so do NOT rawset self.
        __newindex = function(vec, key, value)
            if type(key) ~= "number" then
                local vector_index = self.vector_ordering[key]
                rawset(vec, vector_index, value)
            else
                rawset(vec, key, value)
            end
        end
    })
end

--- Obtain the local distance weighting for each axis, in vector order, for a given point
--- @generic ColourRepresentation
--- @param colour_point ColourRepresentation
--- @return number[]
function coordinates.system:get_axis_weights_for(colour_point)
    return vim.tbl_map(function(distance_weight_fn)
        return distance_weight_fn(colour_point)
    end, self.ordered_distance_weights)
end

--- Get the number of axes for this coordinate system.
--- @return number
function coordinates.system:get_dimensionality()
    return self.axis_count
end

--- Obtain the squared weighted euclidean distance between two, pre-vectorized points. This is
--- more efficient but you must provide vectors that are valid for this coordinate system
--- @param vec_from psh.colourspaces.coordinates.Vector
--- @param vec_to psh.colourspaces.coordinates.Vector
--- @param cached_weights number[]
--- @return number sqaured_distance
function coordinates.system:v_get_distance_squared_with_cached_weights(vec_from, vec_to, cached_weights)
    local distance = 0
    for i = 1, self.axis_count do
        local raw_delta = vec_from[i] - vec_to[i]
        local weighted_delta = cached_weights[i] * raw_delta
        distance = distance + weighted_delta * weighted_delta
    end
    return distance
end

--- Obtain the squared weighted euclidean distance between two points, weighted using the
--- weighting metric of a third point that has been pre-calculated using
--- coordinates.system:get_axis_weights_for()
--- @generic ColourRepresentation
--- @param from ColourRepresentation
--- @param to ColourRepresentation
--- @param cached_weights number[]
--- @return number squared_distance
function coordinates.system:get_distance_squared_with_cached_weights(from, to, cached_weights)
    return self:v_get_distance_squared_with_cached_weights(
        self:make_vector(from),
        self:make_vector(to),
        cached_weights
    )
end

--- Obtain the squared weighted euclidean distance between two, pre-vectorized points,
--- weighted using the weighting metric of a third, non-pre-vectorized point.
--- @generic ColourRepresentation
--- @param v_from psh.colourspaces.coordinates.Vector
--- @param v_to psh.colourspaces.coordinates.Vector
--- @param weighting_point ColourRepresentation
--- @return number squared_distance
function coordinates.system:v_get_distance_squared(v_from, v_to, weighting_point)
    local vector_weights = self:get_axis_weights_for(weighting_point)
    return self:v_get_distance_squared_with_cached_weights(v_from, v_to, vector_weights)
end

--- Obtain the squared weighted euclidean distance between two points, weighted using the
--- weighting metric  of a third point ^.^
--- @generic ColourRepresentation
--- @param from ColourRepresentation
--- @param to ColourRepresentation
--- @param weighting_point ColourRepresentation
--- @return number squared_distance
function coordinates.system:get_distance_squared(from, to, weighting_point)
    local vector_weights = self:get_axis_weights_for(weighting_point)
    return self:v_get_distance_squared_with_cached_weights(
        self:make_vector(from),
        self:make_vector(to),
        vector_weights
    )
end

---@class psh.colourspaces.coordinates.Region<ColourRepresentation>
--- Region of space within a coordinate system, defined by two points - a point closest to
--- the minimum along each axis, and a point closest to the maximum along each axis.
---@field private v_min_coordinates psh.colourspaces.coordinates.Vector Vectorized minimum coords nya
---@field private v_max_coordinates psh.colourspaces.coordinates.Vector Vectorized maximum coords ^.^
---@field private coordinate_system psh.colourspaces.coordinates.System Coordinate system containing the region.
---@field private dimensionality_cache number cached dimensionality.
coordinates.region = {}

--- Construct a region from pre-vectorized coordinates that are already correctly sorted so the
--- minimum is in the first and the maximum is in the second. This performs no checks or reordering,
--- and if you do not make sure the vectors are properly ordered it *will* cause problems.
---@generic ColourRepresentation
---@param coordinate_system psh.colourspaces.coordinates.System<`ColourRepresentation`> Coordinate system that this region is in.
---@param v_min_coordinates psh.colourspaces.coordinates.Vector Min-scale coordinates of the system.
---@param v_max_coordinates psh.colourspaces.coordinates.Vector Max-scale coordinates of the system nya
---@return psh.colourspaces.coordinates.Region<ColourRepresentation>
function coordinates.region.v_raw_new(coordinate_system, v_min_coordinates, v_max_coordinates)
    return setmetatable({
        v_min_coordinates = v_min_coordinates,
        v_max_coordinates = v_max_coordinates,
        coordinate_system = coordinate_system,
        dimensionality_cache = coordinate_system:get_dimensionality()
    }, { __index = coordinates.region })
end

--- Construst a region from pre-vectorized coordinates. This checks that the coordinates are in
--- a correct order. If they are not, it will error()
---@generic ColourRepresentation
---@param coordinate_system psh.colourspaces.coordinates.System<`ColourRepresentation`> Coordinate system that this region is in.
---@param v_min_coordinates psh.colourspaces.coordinates.Vector Min-scale coordinates of the system.
---@param v_max_coordinates psh.colourspaces.coordinates.Vector Max-scale coordinates of the system nya
---@return psh.colourspaces.coordinates.Region<ColourRepresentation>
function coordinates.region.v_checked_new(coordinate_system, v_min_coordinates, v_max_coordinates)
    -- Can't use the cache here since we aren't init yet
    for axis_idx = 1, coordinate_system:get_dimensionality() do
        if v_min_coordinates[axis_idx] > v_max_coordinates[axis_idx] then
            error("Invalid region point ordering on axis # " .. axis_idx)
        end
    end
    return coordinates.region.v_raw_new(coordinate_system, v_min_coordinates, v_max_coordinates)
end

--- Construct a region from pre-vectorized coordinates. This will re-order coordinates along
--- each axis to be in correct order.
---@generic ColourRepresentation
---@param coordinate_system psh.colourspaces.coordinates.System<`ColourRepresentation`> Coordinate system that this region is in.
---@param v_coordinates_first psh.colourspaces.coordinates.Vector One corner of the regions
---@param v_coordinates_last psh.colourspaces.coordinates.Vector Another corner of the regions.
---@return psh.colourspaces.coordinates.Region<ColourRepresentation>
function coordinates.region.v_new(coordinate_system, v_coordinates_first, v_coordinates_last)
    local v_min_coordinates = {}
    local v_max_coordinates = {}
    -- Can't use the cache here since we aren't init yet nyaa
    for axis_idx = 1, coordinate_system:get_dimensionality() do
        local first = v_coordinates_first[axis_idx]
        local last = v_coordinates_last[axis_idx]
        v_min_coordinates[axis_idx] = math.min(first, last)
        v_max_coordinates[axis_idx] = math.max(first, last)
    end
    return coordinates.region.v_raw_new(coordinate_system, v_min_coordinates, v_max_coordinates)
end

--- Get a region that encompasses the entire coordinate systems. If your axes have upper/lower bounds out of order, then this will error.
--- @return psh.colourspaces.coordinates.Region
function coordinates.system:get_global_region()
    local v_min = {}
    local v_max = {}
    for i = 1, self:get_dimensionality() do
        v_min[i] = self.ordered_axes[i]:get_lower_bound()
        v_max[i] = self.ordered_axes[i]:get_upper_bound()
    end
    return coordinates.region.v_checked_new(self, v_min, v_max)
end

--- Get the middle of this region as a vectorized colour point nya
--- @return psh.colourspaces.coordinates.Vector
function coordinates.region:midpoint()
    local v_mid = {}
    for i = 1, self.dimensionality_cache do
        v_mid[i] = (self.v_min_coordinates[i] + self.v_max_coordinates[i]) / 2
    end
    return v_mid
end

--- Get the coordinate system this region is contained in
---@return psh.colourspaces.coordinates.System coordinate_system
function coordinates.region:get_coordinate_system()
    return self.coordinate_system
end

--- Get the volume-like metric of this region, without using coordinate weights - that is, using
--- bare coordinate differences and multiplying ^.^
---@return number
function coordinates.region:bare_metric()
    local metric = 1
    for i = 1, self.dimensionality_cache do
        metric = metric * (self.v_max_coordinates[i] - self.v_min_coordinates[i])
    end
    return metric
end

--- Get the volume-like metric of this region, using the given cached coordinate weights (as produced using
--- the coordinate system this is contained in.
--- @param weights number[] Cached weights to use.
function coordinates.region:weighted_metric_with_cached_weights(weights)
    local metric = 1
    for i = 1, self.dimensionality_cache do
        -- Note that this is equivalent to multiplying the weights together separately and multiplying with the bare
        -- metric.
        metric = metric * (self.v_max_coordinates[i] - self.v_min_coordinates[i]) * weights[i]
    end
    return metric
end

function coordinates.region:clone()
    return coordinates.region.v_new(self.coordinate_system, vim.deepcopy(self.v_min_coordinates),
        vim.deepcopy(self.v_max_coordinates))
end

--- Generate a region from a collection of section bounds, a map of which section you are currently in for each dimension, and
--- a coordinate system and dimensionality.
---@generic ColourRepresentation
---@param coordinate_system psh.colourspaces.coordinates.System<`ColourRepresentation`>
---@param section_index_for_each_dimension integer[] The current section each axis is in.
---@param bounds_for_each_axis_and_section integer[][][] List of minimum and maximum bounds for each section along each axis. Innermost is {[1] = min, [2] = max},
---   next level up is a map from 1 to the section-count of minmax pairs, and the outer map is from each dimension to it's list of minmax pairs.
---@param dimensionality integer number of axes. While this can be obtained from the coordinate system, passing it in is probably more efficient.
---@return psh.colourspaces.coordinates.Region<ColourRepresentation> new_region Generated region
local function make_subregion_from_per_axis_and_section_minmax_bounds(coordinate_system, section_index_for_each_dimension,
                                                                      bounds_for_each_axis_and_section, dimensionality)
    local v_section_min = {}
    local v_section_max = {}
    for dim = 1, dimensionality do
        local section_idx_for_axis = section_index_for_each_dimension[dim]
        local minmax_pair_for_axis_and_section = bounds_for_each_axis_and_section[dim][section_idx_for_axis]
        v_section_min[dim] = minmax_pair_for_axis_and_section[1]
        v_section_max[dim] = minmax_pair_for_axis_and_section[2]
    end
    return coordinates.region.v_raw_new(coordinate_system, v_section_min, v_section_max)
end

--- Implementation of base(section) decrementation of one-indexed axis-length arrays starting from the end.
--  We make a separate function for the hope of LuaJIT optimizing this.
---@param n_dimensional_axis_section_index integer[] Current array of which section each axis is on. All values must be > 0
---@param dimensionality integer number of axes in the state.
---@param sections_per_axis integer how many different sections each axis is split into nya. Used as the "base" of
---  the integer that would be represented by the n-dimensional array.
---@return boolean did_revert_to_start If we reached the end of the overall space and flipped the array all the
--- way back to the last section.
local function decrement_n_dimensional_split(n_dimensional_axis_section_index, dimensionality, sections_per_axis)
    for dim = dimensionality, 1, -1 do
        local new_v = n_dimensional_axis_section_index[dim] - 1
        if new_v ~= 0 then
            -- We reached the most-significant section digit, no more flipping over must occur nya
            n_dimensional_axis_section_index[dim] = new_v
            return false
        else
            -- Flip the section of the current dimension and move to the next, even more significant one nya
            new_v = sections_per_axis
            n_dimensional_axis_section_index[dim] = new_v
        end
    end
    -- The loop did not break even at the most-significant dimensionality. As such, it must have flipped over.
    return true
end


--- Create a new collection of regions by splitting this region into equal amounts along each axis,
--- and apply a function to each new region as well as the index it would have in a hypothetical array. 
--- The index runs backwards so that you can avoid endless reallocation if necessary, or use an existing 
--- array. However, more complex storage methods may be desired as well, for example calculating cumulative
--- probabilities and using the [index method](https://en.wikipedia.org/wiki/Indexed_search)
--- @param sections_per_axis integer Integer > 0 - how many sections each axis should be split into.
---     The total number of resulting regions is sections_per_axis^N (where N is the number of dimensions of
---     the coordinate system containing this.
--- @param subregion_processor fun(backward_array_index: integer, subregion: psh.colourspaces.coordinates.Region)
function coordinates.region:uniform_split_mapping(sections_per_axis, subregion_processor)
    -- I wish we could pre-allocate tables :( nya
    -- Actually I just wish I was writing rust code instead ;-;
    -- To avoid constant re-allocation and copies, we work *backwards*. Whenever
    -- we hit the "maximum" point of lua array optimization, one allocation should happen,
    -- and then everything should work fine. Most probably, the result will always be within this
    -- threshold (iirc it's something like 2^26 elements (67 million or so)), and we can avoid
    -- allocation and copying nya

    -- These are section indices, 1-based, along each vectorized axis.
    ---@type integer[]
    local section_indices = {}

    --- Overall index in the large uber array.
    local current_index = math.pow(sections_per_axis, self.dimensionality_cache)

    --- A list of pairs of index bounds for each axis.
    --- each dimension has one element. This element is an array of pairs, one pair for each
    --- segment along that axis. The first element of the pair is the lower point, the second is the
    --- higher point nya
    ---@type number[][][]
    local axis_bounds = {}

    for dim = 1, self.dimensionality_cache do
        -- Start from the end here.
        section_indices[dim] = sections_per_axis
        local section_minmax_pairs_for_axis = {} -- This will be filled with pairs nya
        local min_coordinate_on_axis = self.v_min_coordinates[dim]
        local max_coordinate_on_axis = self.v_max_coordinates[dim]
        local axis_delta = (max_coordinate_on_axis - min_coordinate_on_axis) / sections_per_axis
        -- Generate minmax bounds.
        -- For the first and last sections we always use the raw values inside our vector, for exactness nya
        for section_along_this_axis = 1, sections_per_axis do
            local min_point_on_axis_for_section = min_coordinate_on_axis + (sections_per_axis - 1) * axis_delta
            local max_point_on_axis_for_section = min_coordinate_on_axis + sections_per_axis * axis_delta
            -- In case we are on an edge, make it use exact values from ourself.
            if section_along_this_axis == 1 then
                min_point_on_axis_for_section = min_coordinate_on_axis
            end
            if section_along_this_axis == sections_per_axis then
                max_point_on_axis_for_section = max_coordinate_on_axis
            end
            section_minmax_pairs_for_axis[section_along_this_axis] = { min_point_on_axis_for_section,
                max_point_on_axis_for_section }
        end
        axis_bounds[dim] = section_minmax_pairs_for_axis
    end

    local was_last = false
    -- These SHOULD be equivalent conditions.
    while current_index > 0 and not was_last do
        local new_region = make_subregion_from_per_axis_and_section_minmax_bounds(self.coordinate_system, section_indices,
        axis_bounds, self.dimensionality_cache)
        subregion_processor(current_index, new_region)
        current_index = current_index - 1
        was_last = decrement_n_dimensional_split(section_indices, self.dimensionality_cache, sections_per_axis)
    end
end


return coordinates

-- programming-socks-highlight.nvim
-- Copyright (C) 2023  sapient_cogbag <sapient_cogbag at protonmail dot com>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published
-- by the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
